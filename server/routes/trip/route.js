/*
 Tripper
 ----------------------
 Author:         Nagendra Dwarakinath
 Created Date:   21st Dec 2013
 Purpose:

 Supported By:
 Reviewed By:

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */

'use strict';

/* node API routes for trip */
function setup(app, handlers){

    app.post('/api/register', handlers.login.regsiter);
    app.post('/api/login', handlers.login.login);
    app.get('/api/questions', handlers.message.getAllQuestion);
    app.post('/api/questions', handlers.message.createQuestion);
    app.get('/api/answers/:qId', handlers.message.updateQuestion);

    app.post('/api/dispatchPassenger', handlers.trip.saveDispatch);
    app.get('/api/dispatchPassenger', handlers.trip.getDispatchData);

 
    app.post('/api/passenger', handlers.trip.saveUser);
    app.put('/api/updatePassenger', handlers.trip.updateUserDetails);
    app.get('/api/passenger', handlers.trip.getAllUser);
    app.delete('/api/passenger', handlers.trip.removeAllUserDetails);
    app.put('/api/passengerRadius', handlers.trip.getAllUserDetailsRadius);
    app.delete('/api/passenger/:id', handlers.trip.removeUserDetails);

    app.post('/api/trip', handlers.trip.save);
    app.get('/api/trip', handlers.trip.getAll);
    app.put('/api/trip', handlers.trip.update);
    app.delete('/api/trip/:name', handlers.trip.remove);
};

module.exports.setup = setup;