

var mongoose = require('mongoose');

var dispatchPassengerSchema = mongoose.Schema({

  ID: {type: Number},
  Passengers: [],
	Status: { type: String },
	TravelsName: { type: String },
	EscortName: { type: String },
	IsEscorted: { type: Boolean },
	CabNumber: { type: String },
	CabType: { type: String }
});

dispatchPassengerSchema.index({ Coordinates: "2d" });

var dispatchPassengerModel = mongoose.model('dispatchPassengerModel', dispatchPassengerSchema);
module.exports = dispatchPassengerModel;
