

var mongoose = require('mongoose');

var tripSchema = mongoose.Schema({
    UserId: {type: String},
    UserName: {type: String},
    StartLocation: [{ Longitude: { type: Number }, Latitude: { type: Number } }],
    StopLocation: [{ Longitude: { type: Number }, Latitude: { type: Number } }],
    StartTime: {type: String, 'default': null},
    StopTime: {type: String, 'default': null},
    StartAddress: {type: String, 'default': null},
    StopAddress: {type: String, 'default': null},
    TripData:[{Longitude: {type:Number}, Latitude: {type: Number}}]
});

var tripModel = mongoose.model('TripModel', tripSchema);

module.exports = tripModel;
