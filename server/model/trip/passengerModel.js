

var mongoose = require('mongoose');

var passengerSchema = mongoose.Schema({

  ID: {type: Number},
	PassengerName:{type: String},
	EmployeeNumber:{type: String},
    Project: {type: String},
    Phone: {type: String},
	StartTime:{type: String},
	StartTimeInMinute:{type: Number},
	Location: {type: String},
	Coordinates: [],
	CurrentDate:{type: String},
	Status:{type: String}
});
passengerSchema.index( {Coordinates : "2d"});
var passengerModel = mongoose.model('passengerModel', passengerSchema);

module.exports = passengerModel;
