

var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    UserId: {type: String},
    FirstName:{type: String},
    LastName:{type: String},
    UserName: {type: String},
    Password:{type: String},
    CreatedDate:{type: String},
    Email:{type: String},
    SessionId:{type: String}
});

var userModel = mongoose.model('userModel', userSchema);

module.exports = userModel;
