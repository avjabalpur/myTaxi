/*
 Tripper
 ----------------------
 Author:         Nagendra Dwarakinath
 Created Date:   21st Dec 2013
 Purpose:

 Supported By:
 Reviewed By:

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */

var logger = require('../../../utils/logger');
var MessageRepository = require('../repositories/messageRepository');
var messageRepository = new MessageRepository();

var MessageHandler = function () {

    this.getAllQuestion = getAllQuestion;
    this.createQuestion = createQuestion;
    this.updateQuestion = updateQuestion;
};

/* event definition for get all trip information */
function getAllQuestion(request, response) {

    messageRepository.getAllQuestion()
        .then(function (promises) {
            logger.log('success: ', 'data fetched');
            return response.json({ results: JSON.parse(promises) });
        },
        function (err) {
            logger.log('error: ' + err, 'pass');
            response.json(400, { error: error.message });
        }
    );
}

/* event definition to save trip data */
function createQuestion(request, response) {

    messageRepository.createQuestion(request.body)
        .then(function (promise) {
            logger.log('success: ', 'data saved');
            //console.log(promise._id);
            var returnValue = "{\"id\":" + JSON.stringify(promise._id) + "}";
            response.json(201,JSON.parse(returnValue));
        },
        function (error) {
            logger.log('error: ', error.stack);
            response.json(400, { error: error.message });
        }
    );
}

/* event definition to update trip data */
function updateQuestion(request, response) {

    messageRepository.updateQuestion(request.body)
        .then(function () {
            logger.log('success: ', 'data updated');
            response.json(201, null);
        },
        function (error) {
            logger.log('error: ', error.stack);
            response.json(400, { error: error.message });
        }
    );
}

module.exports = MessageHandler;

