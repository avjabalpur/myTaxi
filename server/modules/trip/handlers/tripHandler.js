/*
 Tripper
 ----------------------
 Author:         Nagendra Dwarakinath
 Created Date:   21st Dec 2013
 Purpose:

 Supported By:
 Reviewed By:

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */

var logger = require('../../../utils/logger');
var TripRepository = require('../repositories/tripRepository');
var tripRepository = new TripRepository();

var TripHandler = function () {

    this.save = handleCreateRequest;
    this.getAll = handleGetRequest;
    this.update = handleUpdateRequest;
    this.remove = handleDeleteRequest;
    this.saveUser = handleSaveUserDetails;
    this.getAllUser = handlegetAllUserDetails;
    this.getAllUserDetailsRadius = handlegetAllUserDetailsRadius;
    this.removeAllUserDetails = handleRemoveAllUserDetails;
    this.removeUserDetails = handleRemoveUserDetails;
    this.updateUserDetails = handleUpdateUserDetails;

    this.saveDispatch = handleSaveDispatchDetails;
    this.getDispatchData = handleGetAllDispatchDetails;
};


/* event definition to save dispatch data */
function handleSaveDispatchDetails(request, response) {

  tripRepository.saveDispatch(request.body)
      .then(function (promise) {
        logger.log('success: ', 'data saved');
        var returnValue = "{\"id\":" + JSON.stringify(promise._id) + "}";
        response.json(201, JSON.parse(returnValue));
      },
      function (error) {
        logger.log('error: ', error.stack);
        response.json(400, { error: error.message });
      }
  );
}
/* event definition for getting all dispatch information */
function handleGetAllDispatchDetails(request, response) {

  tripRepository.getDispatchData()
      .then(function (promises) {
        logger.log('success: ', 'data fetched');
        return response.json({ results: JSON.parse(promises) });
      },
      function (err) {
        logger.log('error: ' + err, 'pass');
        response.json(400, { error: error.message });
      }
  );
}

/* event definition to save trip data */
function handleRemoveAllUserDetails(request, response) {

    tripRepository.removeAllUserDetails()
        .then(function (promise) {
            return response.json({ results: 'success' });
        },
        function (error) {
            logger.log('error: ', error.stack);
            response.json(400, { error: error.message });
        }
    );
}

/* event definition to save trip data */
function handleRemoveUserDetails(request, response) {

    tripRepository.removeUserDetails(request.params.id)
        .then(function (promise) {
            return response.json({ results: 'success' });
        },
        function (error) {
            logger.log('error: ', error.stack);
            response.json(400, { error: error.message });
        }
    );
}

/* event definition to save trip data */
function handleSaveUserDetails(request, response) {

    tripRepository.saveUser(request.body)
        .then(function (promise) {
            logger.log('success: ', 'data saved');
            //console.log(promise._id);
            var returnValue = "{\"id\":" + JSON.stringify(promise._id) + "}";
            response.json(201,JSON.parse(returnValue));
        },
        function (error) {
            logger.log('error: ', error.stack);
            response.json(400, { error: error.message });
        }
    );
}

/* event definition to save trip data */
function handleUpdateUserDetails(request, response) {

    tripRepository.updateUserDetails(request.body)
        .then(function (promise) {
            logger.log('success: ', 'data updated');
             return response.json({ results: JSON.parse(promise) });
        },
        function (error) {
            logger.log('error: ', error.stack);
            response.json(400, { error: error.message });
        }
    );
}

/* event definition for get all trip information */
function handlegetAllUserDetails(request, response) {

    tripRepository.getAllUser()
        .then(function (promises) {
            logger.log('success: ', 'data fetched');
            return response.json({ results: JSON.parse(promises) });
        },
        function (err) {
            logger.log('error: ' + err, 'pass');
            response.json(400, { error: error.message });
        }
    );
}

/* event definition for get all trip information */
function handlegetAllUserDetailsRadius(request, response) {

    tripRepository.getAllUserDetailsRadius(request.body)
        .then(function (promises) {
            logger.log('success: ', 'data fetched');
            return response.json({ results: JSON.parse(promises) });
        },
        function (err) {
            logger.log('error: ' + err, 'pass');
            response.json(400, { error: error.message });
        }
    );
}



/* event definition to save trip data */
function handleCreateRequest(request, response) {

    tripRepository.save(request.body)
	    .then(function (promise) {
	        logger.log('success: ', 'data saved');
	        //console.log(promise._id);
	        var returnValue = "{\"id\":" + JSON.stringify(promise._id) + "}";
		    response.json(201,JSON.parse(returnValue));
		},
		function (error) {
		    logger.log('error: ', error.stack);
		    response.json(400, { error: error.message });
		}
	);
}

/* event definition for get all trip information */
function handleGetRequest(request, response) {

    tripRepository.getAll()
        .then(function (promises) {
            logger.log('success: ', 'data fetched');
            return response.json({ results: JSON.parse(promises) });
        },
        function (err) {
            logger.log('error: ' + err, 'pass');
            response.json(400, { error: error.message });
        }
    );
}

/* event definition to update trip data */
function handleUpdateRequest(request, response) {
console.log(request.body);
    tripRepository.update(request.body)
	    .then(function () {
		    logger.log('success: ', 'data updated');
		    response.json(201, null);
		},
		function (error) {
		    logger.log('error: ', error.stack);
		    response.json(400, { error: error.message });
		}
	);
}

/* event definition to delete trip details*/
function handleDeleteRequest(request, response) {

    tripRepository.remove(request.params.name)
        .then(function (promise) {
            logger.log('success', 'data deleted');
            return response.json({ results: JSON.parse(promise) });
        },
        function (error) {
            logger.log('error:', error.stack);
            response.json(400, { error: error.message });
        }
    );
}

module.exports = TripHandler;

