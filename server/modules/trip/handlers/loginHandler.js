/*
 Tripper
 ----------------------
 Author:         veerabhadra sajjan
 Created Date:   23rd Aug 2014
 Purpose:

 Supported By:
 Reviewed By:

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */

var logger = require('../../../utils/logger');
var LoginRepository = require('../repositories/loginRepository');
var loginRepository = new LoginRepository();

var LoginHandler = function () {

    this.regsiter = handleCreateRequest;
    this.login = handleGetRequest;
};

/* event definition to save trip data */
function handleCreateRequest(request, response) {

    loginRepository.saveUser(request.body)
	    .then(function (promise) {
	        logger.log('success: ', 'data saved');
	        //console.log(promise._id);
	        var returnValue = "{\"id\":" + JSON.stringify(promise._id) + "}";
		    response.json(201,JSON.parse(returnValue));
		},
		function (error) {
		    logger.log('error: ', error.stack);
		    response.json(400, { error: error.message });
		}
	);
}

/* event definition for get all trip information */
function handleGetRequest(request, response) {

    loginRepository.getSession(request.body)
        .then(function (promises) {
            logger.log('success: ', 'data fetched');
            return response.json({ results: JSON.parse(promises) });
        },
        function (err) {
            logger.log('error: ' + err, 'pass');
            response.json(400, { error: error.message });
        }
    );
}



module.exports = LoginHandler;

