/*
 Tripper
 ----------------------
 Author:         Nagendra Dwarakinath
 Created Date:   21st Dec 2013
 Purpose:

 Supported By:
 Reviewed By:

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */

 var logger = require('../../../utils/logger');
 var mongoose = require('mongoose');
 mongoose.connect('mongodb://localhost/triplogger');
//mongoose.connect('mongodb://tripper:tripper@ds061148.mongolab.com:61148/tripper');
//mongoose.connect('mongodb://triplogger:triplogger@ds059938.mongolab.com:59938/triplogger');
var tripModel = require('../../../model/trip/tripModel');
var passengerModel = require('../../../model/trip/passengerModel');
var dispatchPassengerModel = require('../../../model/trip/dispatchPassengerModel');
var Q = require('q');

function TripRepository() {

  this.save = saveTripDetails;
  this.getAll = getTripDetails;
  this.update = updateTripDetails;
  this.remove = deleteTripDetails;
  this.saveUser = saveUserDetails;
  this.getAllUser = getAllUserDetails;
  this.getAllUserDetailsRadius = getAllUserDetailsRadius;
  this.removeAllUserDetails = removeAllUserDetails;
  this.removeUserDetails = removeUserDetails;
  this.removeUserDetails = removeUserDetails;
  this.updateUserDetails = updateUserDetails;

  this.saveDispatch = saveDispatchDetails;
  this.getDispatchData = getDispatchDetails;
  
}


function getDispatchDetails() {

  var deferred = Q.defer();

  dispatchPassengerModel.find({}, function (err, data) {
    if (err) {
      logger.log('error: ', 'failed to fetch data');
      deferred.reject(new Error(err));
    }
    else {
      logger.log('success: ', 'data fetched');
      deferred.resolve((JSON.stringify(data)));
    }
  });

  return deferred.promise;
}

function saveDispatchDetails(data) {

  var deferred = Q.defer();

  dispatchPassengerModel.find({}, function (err, response) {

    var dispatachPassengerDetails = new dispatchPassengerModel({
      ID: response.length + 1,
      Passengers: data.Passengers,
      Status: data.Status,
      TravelsName: data.TravelsName,
      EscortName: data.EscortName,
      IsEscorted: data.IsEscorted,
      CabNumber: data.CabNumber,
      CabType: data.CabType
    });

    dispatachPassengerDetails.save(function (err, res) {
      if (err) {
        logger.log('error: ', err);
        deferred.reject(new Error(err));
      }
      else {
        logger.log("success: " + 'data saved');
        deferred.resolve(res);
      }
    });
  });
  return deferred.promise;
}

function removeAllUserDetails() {
  var deferred = Q.defer();
  passengerModel.remove({Status: { $ne: "Close"}},function(err, res) {
    if (err) {
      console.log('error: ', err);
      deferred.reject(new Error(err));
    }
    else {
      console.log("success: " + 'data remove');
      deferred.resolve(res);
    }
  });
  return deferred.promise;
}

function removeUserDetails(id) {
  var deferred = Q.defer();
  passengerModel.remove({ID:id},function(err, res) {
    if (err) {
      console.log('error: ', err);
      deferred.reject(new Error(err));
    }
    else {
      console.log("success: " + 'data remove');
      deferred.resolve(res);
    }
  });
  return deferred.promise;
}


function saveUserDetails(data) {

  var deferred = Q.defer();
  passengerModel.aggregate({ $sort: {ID: -1} }, function(err, response) {

    var passengerDetails = new passengerModel({
      ID:(response.length > 0)? response[0].ID + 1 : 1,
      PassengerName:data.PassengerName,
      EmployeeNumber: data.EmployeeNumber,
      Project: data.Project,
      Phone: data.Phone,
      StartTime:data.StartTime,
      StartTimeInMinute:data.StartTimeInMinute,
      Location: data.Location,
      Coordinates: data.Coordinates,
      CurrentDate:data.CurrentDate,
      Status:data.Status
    });

    passengerDetails.save(function(err, res) {
      if (err) {
        logger.log('error: ', err);
        deferred.reject(new Error(err));
      }
      else {
        logger.log("success: " + 'data saved');
        deferred.resolve(res);
      }
    });
  });
  return deferred.promise;
}

/* function definition to update trip data */
function updateUserDetails(data) {
console.log(JSON.stringify(data));
  var deferred = Q.defer();

  passengerModel.update({ID:{$in:data} }, 
    { $set: { Status: 'Close'} },{ multi: true }, function (err, res) {
    if (err) {
      logger.log('error: ', err);
      console.log(JSON.stringify(err));
      deferred.reject(new Error(err));
    }
    else {
      logger.log("success: " + 'data updated');
      deferred.resolve(res);
    }
  });

  return deferred.promise;
}

function getAllUserDetails() {

  var deferred = Q.defer();

  passengerModel.find({}, function(err, data) {
    if (err) {
      logger.log('error: ', 'failed to fetch data');
      deferred.reject(new Error(err));
    }
    else {
      logger.log('success: ', 'data fetched');
      deferred.resolve((JSON.stringify(data)));
    }
  });

  return deferred.promise;
}

function getAllUserDetailsRadius(data) {

  var deferred = Q.defer();
  var StartTime= data.startTime||(data.StartTimeInMinute -30);
  var EndTime= data.endTime||(data.StartTimeInMinute + 30);
  var obj= {
    $geoNear: {
      near:data.coordinates,
      distanceField: "dist.calculated",
      maxDistance: data.noOfKM/111.12,
      //distanceMultiplier: 69.0468,
      uniqueDocs: true,
      query:  { ID: { $nin: data.stored } , StartTimeInMinute: {$gte:StartTime , $lt: EndTime },Status: { $ne: "Close"} },
      limit : data.noOfPersons

    }
  };

  passengerModel.aggregate(obj, function (err, items) {
    if (err) {
      //console.log('error: ', 'failed to fetch data');
      deferred.reject(new Error(err));
    }
    else {
      //console.log('success: ', 'data fetched');
      deferred.resolve(JSON.stringify(items));
    }
  });
  return deferred.promise;
}



/* function definition to get all trip information */
function getTripDetails() {

  var deferred = Q.defer();

  tripModel.find({}, function(err, data) {
    if (err) {
      logger.log('error: ', 'failed to fetch data');
      deferred.reject(new Error(err));
    }
    else {
      logger.log('success: ', 'data fetched');
      deferred.resolve((JSON.stringify(data)));
    }
  });

  return deferred.promise;
}

/* function definition to save trip data */
function saveTripDetails(data) {

  var deferred = Q.defer();
  var tripDetails = new tripModel({
    UserId: data.UserId,
    UserName: data.UserName,
    StartLocation: data.StartLocation,
    StopLocation: data.StopLocation,
    StartTime: data.StartTime,
    StopTime: data.StopTime,
    StartAddress: data.StartAddress,
    StopAddress: data.StopAddress,
    TripData: data.TripData
  });

  tripDetails.save(function(err, res) {
    if (err) {
      logger.log('error: ', err);
      deferred.reject(new Error(err));
    }
    else {
      logger.log("success: " + 'data saved');
      deferred.resolve(res);
    }
  });

  return deferred.promise;
}

/* function definition to get all trip information */
function getTripDetails() {

  var deferred = Q.defer();

  tripModel.find({}, function(err, data) {
    if (err) {
      logger.log('error: ', 'failed to fetch data');
      deferred.reject(new Error(err));
    }
    else {
      logger.log('success: ', 'data fetched');
      deferred.resolve((JSON.stringify(data)));
    }
  });

  return deferred.promise;
}

/* function definition to update trip data */
function updateTripDetails(data) {

  var deferred = Q.defer();
  var tripDetails = new tripModel({
    UserId: data.UserId,
    UserName: data.UserName,
    StartLocation: data.StartLocation,
    StopLocation: data.StopLocation,
    StartTime: data.StartTime,
    StopTime: data.StopTime,
    StartAddress: data.StartAddress,
    StopAddress: data.StopAddress,
    TripData: data.TripData
  });

  var tripDetailsTemp = tripDetails.toObject();
  delete tripDetailsTemp._id;
  delete tripDetailsTemp.UserId;

  tripModel.update({ UserId: data.UserId }, tripDetailsTemp, { multi: false }, function (err, res) {
    if (err) {
      logger.log('error: ', err);
      deferred.reject(new Error(err));
    }
    else {
      logger.log("success: " + 'data updated');
      deferred.resolve(res);
    }
  });

  return deferred.promise;
}

/* function definition to delete trip data*/
function deleteTripDetails(data) {

  var deferred = Q.defer();

  tripModel.remove({ UserId: data }, function (err, res) {
    if (err) {
      logger.log('error:', err);
      deferred.reject(new Error(err));
    } else {
      logger.log("success: " + 'data deleted');
      deferred.resolve(res);
    }
  });

  return deferred.promise;
}

module.exports = TripRepository;
