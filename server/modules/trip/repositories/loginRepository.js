/*
 Tripper
 ----------------------
 Author:         Nagendra Dwarakinath
 Created Date:   21st Dec 2013
 Purpose:

 Supported By:
 Reviewed By:

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */

var logger = require('../../../utils/logger');
var mongoose = require('mongoose');

//mongoose.connect('mongodb://triplogger:triplogger@ds059938.mongolab.com:59938/triplogger');
var userModel = require('../../../model/trip/userModel');
var Q = require('q');

function LoginRepository() {

	this.saveUser = saveUserDetails;
    this.getSession = getSessionDetails;
}

/* function definition to save trip data */
function saveUserDetails(data) {

	var deferred = Q.defer();
    var userDetails = new userModel({
        UserId: data.UserId,
        FirstName:data.FirstName,
        LastName:data.LastName,
        UserName: data.UserName,
        Password:data.Password,
        CreatedDate:data.CreatedDate,
        Email:data.Email,
        SessionId:data.SessionId
    });

    userDetails.save(function(err, res) {
		if (err) {
            logger.log('error: ', err);
			deferred.reject(new Error(err));
		}
		else {
            logger.log("success: " + 'data saved');
			deferred.resolve(res);
		}
	});

	return deferred.promise;
}

/* function definition to get all trip information */
function getSessionDetails(data) {

    var deferred = Q.defer();

    userModel.find({UserName:data.UserName,Password:data.Password}, function(err, data) {
        if (err) {
            logger.log('error: ', 'failed to fetch data');
            deferred.reject(new Error(err));
        }
        else {
            logger.log('success: ', 'data fetched');
            deferred.resolve((JSON.stringify(data)));
        }
    });

    return deferred.promise;
}

module.exports = LoginRepository;
