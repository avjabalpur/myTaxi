/*
 Tripper
 ----------------------
 Author:         Nagendra Dwarakinath
 Created Date:   21st Dec 2013
 Purpose:

 Supported By:
 Reviewed By:

 Update History
 -------------------------------------------------------------------------------
 Name(developer name)        Date (updated date)     Purpose (description)

 */

var logger = require('../../../utils/logger');
var mongoose = require('mongoose');

//mongoose.connect('mongodb://triplogger:triplogger@ds059938.mongolab.com:59938/triplogger');
var messageModel = require('../../../model/trip/messageModel');
var Q = require('q');

function QuestionRepository() {

    this.getAllQuestion = getAllQuestion;
	this.createQuestion = createQuestion;
    this.updateQuestion = updateQuestion;
    this.removeQuestion = removeQuestion;
}

/* function definition to get all trip information */
function getAllQuestion() {

    var deferred = Q.defer();

    messageModel.find({}, function(err, data) {
        if (err) {
            logger.log('error: ', 'failed to fetch data');
            deferred.reject(new Error(err));
        }
        else {
            logger.log('success: ', 'data fetched');
            deferred.resolve((JSON.stringify(data)));
        }
    });

    return deferred.promise;
}


/* function definition to save trip data */
function createQuestion(data) {

	var deferred = Q.defer();
    var messageModel = new messageModel({
        QId: data.QId,
        Quesion: data.Quesion,
        CreatedUser: data.CreatedUser,
        CreatedDate: data.CreatedDate,
        Answers:data.Answers
    });

    messageModel.save(function(err, res) {
		if (err) {
            logger.log('error: ', err);
			deferred.reject(new Error(err));
		}
		else {
            logger.log("success: " + 'data saved');
			deferred.resolve(res);
		}
	});

	return deferred.promise;
}



/* function definition to update trip data */
function updateQuestion(data) {

    var deferred = Q.defer();
    var messageModel = new messageModel({
        QId: data.QId,
        Quesion: data.Quesion,
        CreatedUser: data.CreatedUser,
        CreatedDate: data.CreatedDate,
        Answers: data.Answers
    });

    var questionDetailsTemp = messageModel.toObject();
    delete questionDetailsTemp._id;
    delete questionDetailsTemp.UserId;

    messageModel.update({ QId: data.QId }, questionDetailsTemp, { multi: false }, function (err, res) {
        if (err) {
            logger.log('error: ', err);
            deferred.reject(new Error(err));
        }
        else {
            logger.log("success: " + 'data updated');
            deferred.resolve(res);
        }
    });

    return deferred.promise;
}

/* function definition to delete trip data*/
function removeQuestion(data) {

    var deferred = Q.defer();

    messageModel.remove({ QId: data }, function (err, res) {
        if (err) {
            logger.log('error:', err);
            deferred.reject(new Error(err));
        } else {
            logger.log("success: " + 'data deleted');
            deferred.resolve(res);
        }
    });

    return deferred.promise;
}

module.exports = QuestionRepository;
