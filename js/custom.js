

window.app.controller('Controller', function ($scope, $http, $window, $timeout) {

  $scope.isDetails = false;
  $scope.isDispatch = false;
  $scope.isReport = false;
  $scope.options1 = null;
  $scope.fromAddresResult = '';
  $scope.fromAddresDetails = '';
  $scope.user = {};

  $scope.allUser = [];
  $scope.stored = [];
  $scope.collection = [];

  $scope.mytime = new Date();
  $scope.hstep = 1;
  $scope.mstep = 30;
  $scope.ismeridian = true;

  $scope.noOfPersons = 3;
  $scope.noOfKM = 10;

  $scope.route = 'http://localhost:5000/';
  //$scope.route = 'http://triplogger.herokuapp.com/';

  $http({
      method: 'GET',
      url: $scope.route + 'api/passenger',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    }).success(function (response, status, header, config) {
        $scope.allPassenger = response.results;;
    }).error(function () {
      alert('error');
    });

    $scope.selectedPassenger = function(passenger)
    {
      
      $scope.user.passengerName = passenger.PassengerName
      $scope.user.employeeNumber = passenger.EmployeeNumber; 
      $scope.user.project = passenger.Project;
      $scope.user.phone = passenger.Phone; 
      $scope.mytime = new Date(passenger.StartTime);
      $scope.fromAddresResult=passenger.Location;
      $scope.location=passenger.Location;
      $scope.fromAddresDetails={
        geometry:{
          location:{k: passenger.Coordinates[0],B:passenger.Coordinates[1]}
        }
      }
    };

    $scope.checkPassenger= function()
    {
     
      var isSelected=false;
      for (var i = 0; i < $scope.allPassenger.length; i++) {
        if($scope.allPassenger[i].PassengerName==$scope.user.passengerName)
        {
          isSelected=true;  
        }
        
      }

      if(isSelected==false)
      {
        $scope.user.employeeNumber = ''; 
        $scope.user.project = '';
        $scope.user.phone = ''; 
        $scope.mytime = new Date();
        $scope.fromAddresResult='';
        $scope.location='';
        $scope.fromAddresDetails={};
      }
    }
  
  
  

  $scope.deleteAll = function () {
    $http({
      method: 'DELETE',
      url: $scope.route + 'api/passenger',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    }).success(function (response, status, header, config) {
      $scope.stored = []; $scope.collection = [];


    }).error(function () {
      alert('error');
    });
  }

  $scope.deletePassenger = function (data) {
    $http({
      method: 'DELETE',
      url: $scope.route + 'api/passenger/' + data.ID,
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    }).success(function (response, status, header, config) {
      $scope.stored = []; $scope.collection = [];
      $scope.getAllPassengerName();

    }).error(function () {
      alert('error');
    });
  }

  $scope.save = function () {

    if($scope.user.passengerName == '' || $scope.fromAddresResult == '')
      {
        alert('Please enter the all details');
        return ;
      }

    var totalMin = ($scope.mytime.getHours() * 60) + $scope.mytime.getMinutes();

    var user = {
      "PassengerName": $scope.user.passengerName,
      "EmployeeNumber": $scope.user.employeeNumber,
      "Project": $scope.user.project,
      "Phone": $scope.user.phone,
      "StartTimeInMinute": totalMin,
      "StartTime": $scope.mytime,
      "CurrentDate": new Date(),
      "Status": "open",
      "Location": $scope.fromAddresResult,
      "Coordinates": [
        $scope.fromAddresDetails.geometry.location.k,
        $scope.fromAddresDetails.geometry.location.B
      ]

    }

    $http({
      method: 'POST',
      url: $scope.route + 'api/passenger',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      data: user
    }).success(function (response, status, header, config) {
      alert('Thank you for booking the Cab\nWe will get back to you shortly.');
      $scope.clear();

    }).error(function () {
      alert('error');
    })
  }

  $scope.updateUserDetail = function (user) {
    
    var ids = [];
    user.data.forEach(function (res) {
      ids.push(res.ID);
    });

    $http({
      method: 'PUT',
      url: $scope.route + 'api/updatePassenger',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      data: ids
    }).success(function (response, status, header, config) {

      alert('Cab dispatch successfully.');
      $scope.getAllPassengerName();
      $scope.getDispatchData();

    }).error(function () {
      alert('error');
    })
  }

  $scope.getAllPassengerName = function () {

    $http({
      method: 'GET',
      url: $scope.route + 'api/passenger',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    }).success(function (response, status, header, config) {

      $scope.allUser = [];
      for (var i = 0; i < response.results.length; i++) {
      if (response.results[i].Status.toUpperCase() != "CLOSE")
        $scope.allUser.push(response.results[i]);
      }


      $scope.stored = []; $scope.collection = [];
      $scope.getNearBy();

    }).error(function () {
      alert('error');
    })

  }

  $scope.clear = function () {
    $scope.user = {};
    $scope.fromAddresDetails = {};
    $scope.fromAddresResult = '';
    $scope.location = '';
    $scope.mytime = new Date();
  };

  $scope.getNearBy = function () {

    if ($scope.allUser.length == 0) return;

    var obj = {
      coordinates: $scope.allUser[0].Coordinates,
      stored: $scope.stored,
      StartTimeInMinute: $scope.allUser[0].StartTimeInMinute,
      noOfKM: parseInt($scope.noOfKM),
      noOfPersons: parseInt($scope.noOfPersons)
    };

    $http({
      method: 'PUT',
      url: $scope.route + 'api/passengerRadius',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      data: obj
    }).success(function (response, status, header, config) {

      response.results.forEach(function (data) {
        $scope.stored.push(data.ID);

        for (var i = 0; i < $scope.allUser.length; i++) {
          if ($scope.allUser[i].ID === data.ID) $scope.allUser.splice(i, 1);
        }

      });
      if (response.results.length == 0)
        $scope.allUser = [];
      else
        $scope.collection.push({ "data": response.results });

      $scope.getNearBy($scope.allUser, $scope.stored, $scope.collection);
    }).error(function () {
      alert('error');
    })
  };

  $scope.showDetails = function () {
    $scope.isDetails = true;
    $scope.getAllPassengerName();
    $scope.getDispatchData();
  };

  $scope.hideDetails = function () {
    $scope.isDetails = false;
  };

  $scope.showReport = function () {
    $scope.isReport = true;

    $http({
      method: 'GET',
      url: $scope.route + 'api/passenger',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    }).success(function (response, status, header, config) {

      $scope.allUser = [];

      for (var i = 0; i < response.results.length; i++) {
        if (response.results[i].Status != "Close")
          $scope.allUser.push(response.results[i]);
      }

    }).error(function () {
      alert('error');
    })
  };

  $scope.printAll = function (result) {
    //$window.open('http://www.fine-der.net', 'galleryWindowSample', 'width=1100,height=600,menubar=no,toolbar=no,location=no,scrollbars=yes');

    $http({
      method: 'GET',
      url: $scope.route + 'api/dispatchPassenger',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    }).success(function (response, status, header, config) {

      var title = 'emids Taxi';
      var data = '';

      data += '<table class="table" border="1">';
      data += '<thead>';
      data += '<tr>';

      data += '<th>Time</th>';
      data += '<th>Emp Name</th>';
      data += '<th>Project</th>';
      data += '<th>Phone</th>';
      data += '<th>Cab No</th>';
      data += '<th>Type</th>';
      data += '<th>Drop Place</th>';
      data += '<th>Escort (Y/N)</th>';
      data += '<th>Escort Name</th>';
      data += '<th>Travels Name</th>';

      data += '</tr>';
      data += '</thead>';

      data += '<tbody>';

      angular.forEach(response.results, function (dispatchPassenger) {

        
        var isEscorted = (dispatchPassenger.IsEscorted === true) ? "Yes" : "No";

        angular.forEach(dispatchPassenger.Passengers, function (result) {


          data += '<tr>';

          data += '<td>' + ((new Date(result.StartTime).getHours() > 12) ? (new Date(result.StartTime).getHours() - 12) : new Date(result.StartTime).getHours()) + ':' + ((new Date(result.StartTime).getMinutes() < 10) ? '00' : new Date(result.StartTime).getMinutes()) + ' ' + ((new Date(result.StartTime).getHours() > 12) ? 'PM' : 'AM') + '</td>';
          data += '<td>' + result.PassengerName + '</td>';
          data += '<td>' + result.Project + '</td>';
          data += '<td>' + result.Phone + '</td>';
          data += '<td>' + dispatchPassenger.CabNumber + '</td>';
          data += '<td>' + dispatchPassenger.CabType + '</td>';
          data += '<td>' + result.Location + '</td>';
          data += '<td>' + isEscorted + '</td>';
          data += '<td>' + dispatchPassenger.EscortName + '</td>';
          data += '<td>' + dispatchPassenger.TravelsName + '</td>';

          data += '</tr>';
        });

      });

      data += '</tbody>';
      data += '</table>';

      var printWindow = $window.open('', '_default', '');
      printWindow.document.write('<html><head><title>emids Taxi</title>');
      printWindow.document.write("<style type='text/css'> body {font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-size: 12px;line-height: 1.42857143;color: #333; background-color: #fff;  } .table{ border-collapse: collapse;width:100%; } .table tr td { padding: 5px;line-height: 1.42857143; } .table tr th { padding: 5px;line-height: 1.42857143;background: #f7f7f7; } </style>");

      printWindow.document.write('</head><body>');
      printWindow.document.write('<br/>');
      printWindow.document.write('<h2>' + title + '</h2>');
      printWindow.document.write('<hr/>');
      printWindow.document.write('<h3> My Taxi Report for the date: ' + new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + (new Date().getYear() + 1900) + '</h3>');
      printWindow.document.write(data);
      printWindow.document.write('</body></html>');

      printWindow.print();
      printWindow.close();

    }).error(function () {
      alert('error');
    })
  }

  $scope.printRow = function (result) {
   
    var title = 'emids Taxi';
      var data = '';

      data += '<table class="table" border="1">';
      data += '<thead>';
      data += '<tr>';

      data += '<th>Time</th>';
      data += '<th>Emp Name</th>';
      data += '<th>Project</th>';
      data += '<th>Phone</th>';
      data += '<th>Cab No</th>';
      data += '<th>Type</th>';
      data += '<th>Drop Place</th>';
      data += '<th>Escort (Y/N)</th>';
      data += '<th>Escort Name</th>';
      data += '<th>Travels Name</th>';

      data += '</tr>';
      data += '</thead>';

      data += '<tbody>';


    angular.forEach(result.data, function (result) {

       data += '<tr>';

          data += '<td>' + ((new Date(result.StartTime).getHours() > 12) ? (new Date(result.StartTime).getHours() - 12) : new Date(result.StartTime).getHours()) + ':' + ((new Date(result.StartTime).getMinutes() < 10) ? '00' : new Date(result.StartTime).getMinutes()) + ' ' + ((new Date(result.StartTime).getHours() > 12) ? 'PM' : 'AM') + '</td>';
          data += '<td>' + result.PassengerName + '</td>';
          data += '<td>' + result.Project + '</td>';
          data += '<td>' + result.Phone + '</td>';
          data += '<td></td>';
          data += '<td></td>';
          data += '<td>' + result.Location + '</td>';
          data += '<td></td>';
          data += '<td></td>';
          data += '<td></td>';

          data += '</tr>';


      /*data += record.CurrentDate;
      data += record.PassengerName;
      data += record.Project;
      data += record.CabNumber;
      data += record.cabType;
      data += record.Location;
      data += record.IsEscorted;
      data += record.EscortName;
      data += record.TravelsName;
      data += record.Phone;
      data += record.Project;*/
    });

      data += '</tbody>';
      data += '</table>';

 var printWindow = $window.open('', '_default', '');
      printWindow.document.write('<html><head><title>emids Taxi</title>');
      printWindow.document.write("<style type='text/css'> body {font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-size: 12px;line-height: 1.42857143;color: #333; background-color: #fff;  } .table{ border-collapse: collapse;width:100%; } .table tr td { padding: 5px;line-height: 1.42857143; } .table tr th { padding: 5px;line-height: 1.42857143;background: #f7f7f7; } </style>");

      printWindow.document.write('</head><body>');

    
    printWindow.document.write('<br/>');
    printWindow.document.write('<h2>' + title + '</h2>');
    printWindow.document.write('<br/><br/>');
    printWindow.document.write(data);
    printWindow.document.write('</body></html>');

	$timeout(function () {
            printWindow.print();
            printWindow.close();
        },50);
    
    
  }

  $scope.printDispatchRow = function (result) {
    
    var title = 'emids Taxi';
    var data = '';

    data += '<table class="table" border="1">';
    data += '<thead>';
    data += '<tr>';

    data += '<th>Time</th>';
    data += '<th>Emp Name</th>';
    data += '<th>Project</th>';
    data += '<th>Phone</th>';
    data += '<th>Cab No</th>';
    data += '<th>Type</th>';
    data += '<th>Drop Place</th>';
    data += '<th>Escort (Y/N)</th>';
    data += '<th>Escort Name</th>';
    data += '<th>Travels Name</th>';

    data += '</tr>';
    data += '</thead>';

    data += '<tbody>';


    angular.forEach(result.Passengers, function (passenger) {

      var isEscorted = (result.IsEscorted === true) ? "Yes" : "No";

      data += '<tr>';

      data += '<td>' + ((new Date(passenger.StartTime).getHours() > 12) ? (new Date(passenger.StartTime).getHours() - 12) : new Date(passenger.StartTime).getHours()) + ':' + ((new Date(passenger.StartTime).getMinutes() < 10) ? '00' : new Date(passenger.StartTime).getMinutes()) + ' ' + ((new Date(passenger.StartTime).getHours() > 12) ? 'PM' : 'AM') + '</td>';
      data += '<td>' + passenger.PassengerName + '</td>';
      data += '<td>' + passenger.Project + '</td>';
      data += '<td>' + passenger.Phone + '</td>';
      data += '<td>' + result.CabNumber + '</td>';
      data += '<td>' + result.CabType + '</td>';
      data += '<td>' + passenger.Location + '</td>';
      data += '<td>' + isEscorted + '</td>';
      data += '<td>' + result.EscortName + '</td>';
      data += '<td>' + result.TravelsName + '</td>';

      data += '</tr>';
    });

    data += '</tbody>';
    data += '</table>';

    var printWindow = $window.open('', '_default', '');
    printWindow.document.write('<html><head><title>emids Taxi</title>');
    printWindow.document.write("<style type='text/css'> body {font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;font-size: 12px;line-height: 1.42857143;color: #333; background-color: #fff;  } .table{ border-collapse: collapse;width:100%; } .table tr td { padding: 5px;line-height: 1.42857143; } .table tr th { padding: 5px;line-height: 1.42857143;background: #f7f7f7; } </style>");

    printWindow.document.write('</head><body>');


    printWindow.document.write('<br/>');
    printWindow.document.write('<h2>' + title + '</h2>');
    printWindow.document.write('<br/><br/>');
    printWindow.document.write(data);
    printWindow.document.write('</body></html>');

    $timeout(function () {
      printWindow.print();
      printWindow.close();
    }, 50);


  }

  $scope.saveDispatchData = function (userData) {

    var dispatch = {
      "Passengers": userData.data,
      "Status": 'Close',
      "TravelsName": userData.travelsName,
      "EscortName": userData.escortName,
      "IsEscorted": (userData.travelsName !== null && userData.travelsName.trim().length > 0) ? true : false,
      "CabNumber": userData.cabNumber,
      "CabType": userData.cabType
    }

    $http({
      method: 'POST',
      url: $scope.route + 'api/dispatchPassenger',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      data: dispatch
    }).success(function (response, status, header, config) {

      $scope.isDispatch = false;
      $scope.dispatchUser = {};
      $scope.updateUserDetail(userData);
      $scope.getDispatchData();

    }).error(function () {
      alert('error');
    })

  }

  $scope.getDispatchData = function () {

    $http({
      method: 'GET',
      url: $scope.route + 'api/dispatchPassenger',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    }).success(function (response, status, header, config) {

      $scope.dispatchPassengerData = [];

      for (var i = 0; i < response.results.length; i++) {
        $scope.dispatchPassengerData.push(response.results[i]);
      }

    }).error(function () {
      alert('error');
    })

  };


});